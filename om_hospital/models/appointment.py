# -*- coding: utf-8 -*-
from odoo import api, fields, models, _, tools
from odoo.exceptions import ValidationError
import random


class HospitalAppointment(models.Model):
    _name = 'hospital.appointment'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Hospital Appointment'
    _rec_name = 'name'
    _order = 'booking_date desc,id desc'

    # @api.model
    # def default_get(self, fields_list):
    #     rst = super(HospitalAppointment, self).default_get(fields_list)
    #     if not rst.get('name'):
    #         rst['name'] = self.env['ir.sequence'].next_by_code('hospital.appointment')
    #         return rst

    name = fields.Char(string="Sequences", required=False, default=lambda self: _('New'))
    patient_id = fields.Many2one(comodel_name="hospital.patient", string="Patient", ondelete='restrict', tracking=1)
    # patient_id = fields.Many2one(comodel_name="hospital.patient", string="Patient", ondelete='cascade')
    appointment_time = fields.Datetime(string="Appointment Time", default=fields.Datetime.now)
    booking_date = fields.Date(string="Booking Date", default=fields.Date.context_today, tracking=4)
    gender = fields.Selection(related='patient_id.gender', readonly=True)
    age = fields.Integer(string="Age", related='patient_id.age', )
    ref = fields.Char(string="Reference", help="test Help")
    prescription = fields.Html(string="Prescription")
    priority = fields.Selection(string="priority", selection=[
        ('0', 'Normal'),
        ('1', 'Low'),
        ('2', 'High'),
        ('3', 'Very High'),
    ])

    state = fields.Selection(string="Status", selection=[
        ('draft', 'Draft'),
        ('start', 'Start'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], default='draft', required=True, tracking=True)
    doctor_id = fields.Many2one(comodel_name="res.users", string="Doctor", required=False, tracking=2)
    pharmacy_line_ids = fields.One2many(comodel_name="appointment.pharmacy.lines", inverse_name="appointment_id",
                                        string="Pharmacy Line", required=False, )
    hide_sales_price = fields.Boolean(string="Hide Sales Price", default=True, )
    reason = fields.Text(string="reason", required=False, tracking=True)
    operation_id = fields.Many2one(comodel_name="hospital.operation", string="Operation", required=False, )
    progress = fields.Integer(string="Progress", required=False, compute='_compute_progress')
    duration = fields.Integer(string="Duration", required=False, tracking=3)
    company_id = fields.Many2one(comodel_name="res.company", string="Company", default=lambda self: self.env.company,
                                 required=False, )
    currency_id = fields.Many2one(comodel_name="res.currency", string="Currency", related='company_id.currency_id',
                                  required=False, )
    amount_total = fields.Monetary(string="Amount Total", required=False, compute='_compute_amount_total',
                                   currency_field='currency_id')



    @api.depends('pharmacy_line_ids.price_subtotal')
    def _compute_amount_total(self):
        for rec in self:
            amount_total = 0.0
            for line in rec.pharmacy_line_ids:
                amount_total += line.price_subtotal
            rec.update({
                'amount_total': amount_total
            })

    @api.onchange('patient_id')
    def onchange_patient_id(self):
        self.ref = self.patient_id.ref

    def action_test(self):
        return {
            'type': 'ir.actions.act_url',
            'target': 'new',
            'url': 'http://www.oscg.cn'
            # 'url': 'self.prescription'

        }

    def action_notification(self):
        action = self.env.ref('om_hospital.action_hospital_patient_view')
        return {
            'type': 'ir.actions.client',
            'tag': 'display_notification',
            'params': {
                'message': 'Button click %s successfull ',
                'title': _('Click to open the patient record'),
                'type': 'success',
                'links': [{
                    'label': "patient " + self.patient_id.name,
                    'url': f'#action={action.id}&id={self.patient_id.id}&model=hospital.patient&view_type=form'
                }],
                'sticky': True,
                'next': {
                    'type': 'ir.actions.act_window',
                    'res_model': 'hospital.patient',
                    'res_id': self.patient_id.id,
                    'views': [(False, 'form')]
                }
            }
        }

    def action_share_whatsapp(self):
        if not self.patient_id.phone:
            raise ValidationError(_("missing phone?"))
        message = 'Hi %s you appointment NO %s' % (self.patient_id.name, self.name)
        whatsapp_api_url = 'http://api.whatsapp.com/send?phone=%s&text=%s' % (self.patient_id.phone, message)
        self.message_post(body='Appointment created Whatsapp Message: ' + message, subject='Whatsapp Message')
        return {
            'type': 'ir.actions.act_url',
            'target': 'new',
            'url': whatsapp_api_url
        }

    def action_send_mail(self):
        template = self.env.ref('om_hospital.appointment_mail_template')
        for rec in self:
            if rec.patient_id.email:
                template.send_mail(rec.id, force_send=True)


    def action_start(self):
        for rec in self:
            if rec.state == 'draft':
                rec.state = 'start'

    # def action_cancel(self):
    #     for rec in self:
    #         rec.state = 'cancel'

    def action_cancel(self):
        action = self.env.ref('om_hospital.action_cancel_appointment').read()[0]
        return action

    def action_draft(self):
        for rec in self:
            rec.state = 'draft'

    def action_done(self):
        for rec in self:
            rec.state = 'done'

        return {
            'effect': {
                'fadeout': 'slow',
                'message': 'Clik Successfull',
                'type': 'rainbow_man'
            }
        }

    @api.depends('state')
    def _compute_progress(self):
        for rec in self:
            if rec.state == "draft":
                rec.progress = random.randrange(0, 25)
            elif rec.state == "start":
                rec.progress = random.randrange(26, 99)
            elif rec.state == "done":
                rec.progress = 100
            else:
                rec.progress = 0

    def set_line_number(self):
        sl_no = 0
        for line in self.pharmacy_line_ids:
            sl_no += 1
            line.sl_no = sl_no

    @api.model
    def create(self, vals_list):
        vals_list['name'] = self.env['ir.sequence'].next_by_code('hospital.appointment')
        res = super(HospitalAppointment, self).create(vals_list)
        res.set_line_number()
        print('createres----->', res)
        print('createres----->', vals_list)
        return res

    def write(self, vals):
        res = super(HospitalAppointment, self).write(vals)
        self.set_line_number()
        print('writeres----->', res)
        print('writeres----->', vals)
        return res

    def unlink(self):
        if self.state != 'draft':
            raise ValidationError(_("you cannot delete %s appointment!", self.state))
        return super(HospitalAppointment, self).unlink()


class AppointmentPharmacyLines(models.Model):
    _name = 'appointment.pharmacy.lines'
    _rec_name = 'product_id'
    _description = 'Appointment Pharmacy Lines'

    sl_no = fields.Integer(string="Number No", required=False, )
    product_id = fields.Many2one(comodel_name="product.product", string="Product", required=True, )
    price_unit = fields.Float(string="Price", related='product_id.list_price')
    qyt = fields.Integer(string="Quantity", required=False, default=1)
    appointment_id = fields.Many2one(comodel_name="hospital.appointment", string="", required=False, )
    currency_id = fields.Many2one(comodel_name="res.currency", string="Currency", related='appointment_id.currency_id',
                                  required=False, )
    price_subtotal = fields.Monetary(string="Subtotla", required=False, compute='_compute_price_subtotal',
                                     currency_field='currency_id')

    @api.depends('price_unit', 'qyt')
    def _compute_price_subtotal(self):
        for rec in self:
            rec.price_subtotal = rec.price_unit * rec.qyt
