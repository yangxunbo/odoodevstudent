# -*- coding: utf-8 -*-
from odoo import api, fields, models, _, tools
from datetime import date
from odoo.exceptions import ValidationError
from dateutil import relativedelta


class HospitalPatient(models.Model):
    _name = 'hospital.patient'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    # _rec_name = 'name'
    _description = 'Hospital Patient'

    name = fields.Char(string="Name", tracking=True, required=True)
    date_of_birth = fields.Date(string="Date Birth", required=False)
    ref = fields.Char(string="Reference", default=lambda self: _('New'))
    age = fields.Integer(string="Age", compute='_compute_age', inverse='_inverse_compute_age', search='_search_age', )
    gender = fields.Selection(string="Gender", selection=[('male', 'Male'), ('female', 'Female')])
    active = fields.Boolean(string="Active", default=True)
    appointment_id = fields.Many2one(comodel_name="hospital.appointment", string="Appointment", required=False, )
    image = fields.Image(string="Image")
    tag_ids = fields.Many2many(comodel_name="patient.tag", relation="", column1="", column2="", string="Tags", )
    appointment_count = fields.Integer(string="Appointment Count", compute='_compute_appointment_count', store=True)
    appointment_ids = fields.One2many(comodel_name="hospital.appointment", inverse_name="patient_id",
                                      string="Appointment", required=False, )
    parent = fields.Char(string="Parent", required=False, )
    marital_status = fields.Selection(string="Marital Status",
                                      selection=[('married', 'Married'), ('single', 'Single'), ],
                                      required=False, tracking=True)
    partner_name = fields.Char(string="Partner Name", required=False, )
    notes = fields.Text(string="Notes", required=False, )
    is_birthday = fields.Boolean(string="Birthday ?", compute='_compute_is_birthday')
    # is_birthday = fields.Boolean(string="Birthday ?",)
    phone = fields.Char(string="Phone", required=False, )
    email = fields.Char(string="E-mail", required=False, )
    website = fields.Char(string="Website", required=False, )

    @api.constrains('date_of_birth')
    def _check_date_of_birth(self):
        for rec in self:
            if rec.date_of_birth and rec.date_of_birth > fields.Date.today():
                raise ValidationError(_("the entered data is today!"))

    @api.ondelete(at_uninstall=False)
    def _check_appointment_ids(self):
        for rec in self:
            if rec.appointment_ids:
                raise ValidationError(_("You cannot delete a patient with appointments!"))

    @api.depends('date_of_birth')
    def _compute_age(self):
        today = date.today()
        for rec in self:
            if rec.date_of_birth:
                rec.age = today.year - rec.date_of_birth.year
            else:
                rec.age = 0

    @api.depends('age')
    def _inverse_compute_age(self):
        today = date.today()
        for rec in self:
            rec.date_of_birth = today - relativedelta.relativedelta(years=rec.age)

    def _search_age(self, operator, value):
        age_of_birth = date.today() - relativedelta.relativedelta(years=value)
        print(age_of_birth, date.today(), relativedelta.relativedelta(years=value))
        start_of_year = age_of_birth.replace(month=1, day=1)
        end_of_year = age_of_birth.replace(month=12, day=31)
        print(start_of_year, end_of_year)
        return [('date_of_birth', '>=', start_of_year), ('date_of_birth', '<=', end_of_year)]

    @api.depends('appointment_ids')
    # def _compute_appointment_count(self):
    #     appointment_group=self.env['hospital.appointment'].sudo().read_group(
    #         domain=[('state', '!=', 'cancel')],
    #         fields=['patient_id'],
    #         groupby=['patient_id']
    #     )
    #     for rec in appointment_group:
    #         print(rec)
    #         patient_id = rec.get('patient_id')[0]
    #         patient_rec = self.browse(patient_id)
    #         print(patient_rec)
    #         patient_rec.appointment_count = rec['patient_id_count']
    #         self -= patient_rec
    #     self.appointment_count = 0
    def _compute_appointment_count(self):
        if self.appointment_ids:
            for rec in self:
                count = self.env['hospital.appointment'].search_count(
                    [('patient_id', '=', rec.id), ('state', '!=', 'cancel')])
                rec.appointment_count = count

    def name_get(self):
        patient_list = []
        for rec in self:
            name = "[%s] - %s" % (rec.ref, rec.name)
            patient_list.append((rec.id, name))
        return patient_list

    def action_test(self):
        return {
            'type': 'ir.actions.act_url',
            'target': 'new',
            'url': 'http://www.oscg.cn'
            # 'url': 'self.prescription'
        }

    @api.depends('date_of_birth')
    def _compute_is_birthday(self):
        for rec in self:
            rec.is_birthday = False
            if rec.date_of_birth:
                if date.today().day == rec.date_of_birth.day and date.today().month == rec.date_of_birth.month:
                    rec.is_birthday = True

    def action_view_appointments(self):
        return {
            'name': _('Appointments'),
            'res_model': 'hospital.appointment',
            'view_id': False,
            'view_mode': 'tree,form,calendar,activity',
            'context': {'default_patient_id': self.id},
            'domain': [('patient_id', '=', self.id), ('state', '!=', 'cancel')],
            'target': 'current',
            'type': 'ir.actions.act_window'
        }

    @api.model
    def create(self, vals_list):
        vals_list['ref'] = self.env['ir.sequence'].next_by_code('hospital.patient')
        return super(HospitalPatient, self).create(vals_list)

    def write(self, vals):
        # if not self.ref and not vals.get('ref'):
        if not self.ref:
            vals['ref'] = self.env['ir.sequence'].next_by_code('hospital.patient')
        return super(HospitalPatient, self).write(vals)
