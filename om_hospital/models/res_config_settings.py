from odoo import fields, models, api


class CancelDayConfig(models.TransientModel):
    _inherit = 'res.config.settings'

    cancel_days = fields.Integer(string="Cancel Days", required=False, config_parameter='om_hospital.cancel_days',)
