from odoo import fields, models, api, _


class ModelName(models.Model):
    _name = 'hospital.operation'
    _description = 'Hospital Operation'
    _rec_name = 'operation_name'
    _log_access = False
    _order = 'sequence,id'

    doctor_id = fields.Many2one(comodel_name="res.users", string="Dcotor", required=False, )
    operation_name = fields.Char(string="Name", required=False, )
    reference_record = fields.Reference(string="Record", selection=[('hospital.patient', 'Patient'),
                                                                    ('hospital.appointment', 'Appointment'), ],
                                        required=False, )
    sequence = fields.Integer(string="Sequence", required=False, default=10)

    @api.model
    def name_create(self, name):
        name_create = self.create({'operation_name': name}).name_get()[0]
        return name_create
