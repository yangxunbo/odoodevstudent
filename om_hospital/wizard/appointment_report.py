from odoo import api, fields, models, _, tools


class AppointmentReportWizard(models.TransientModel):
    _name = 'appointment.report.wizard'
    _description = 'Print Appointment Wizard'

    patient_id = fields.Many2one(comodel_name="hospital.patient", string="Patient", required=False, )
    date_from = fields.Date(string="Date From", required=False, )
    date_to = fields.Date(string="Date To", required=False, )

    def action_print_report(self):
        domain = []
        if self.patient_id:
            domain += [('patient_id', '=', self.patient_id.id)]
        if self.date_from:
            domain += [('appointment_time', '>=', self.date_from)]
        if self.date_to:
            domain += [('appointment_time', '<=', self.date_from)]
        print('domain------->', domain)
        appointments = self.env['hospital.appointment'].search_read(domain)
        data = {
            'form': self.read()[0],
            'appointments': appointments
        }
        print('data----->', data)

        return self.env.ref('om_hospital.action_report_appointment').report_action(self, data=data)
