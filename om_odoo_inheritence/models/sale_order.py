# -*- coding: utf-8 -*-

from odoo import fields, models, api, _


class SaleOrderInherit(models.Model):
    _name = 'sale.order'
    _inherit = 'sale.order'
    confirmed_user_id = fields.Many2one(comodel_name="res.users", string="Confirmed User", required=False, )

    def action_confirm(self):
        super(SaleOrderInherit, self).action_confirm()
        self.confirmed_user_id = self.env.user.id

    def _prepare_invoice(self):
       invoice_vals = super(SaleOrderInherit, self)._prepare_invoice()
       invoice_vals['so_confirmed_user_id'] = self.confirmed_user_id.id
       print('invoice_vals---->',invoice_vals['so_confirmed_user_id'])
       return invoice_vals



