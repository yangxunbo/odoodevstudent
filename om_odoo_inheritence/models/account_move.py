from odoo import fields, models, api


class AccountMove(models.Model):
    _name = 'account.move'
    _inherit = 'account.move'

    so_confirmed_user_id = fields.Many2one(comodel_name="res.users", string="SO Confirmed User", required=False, )


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    line_number = fields.Integer(string="Line Number", required=False, )
